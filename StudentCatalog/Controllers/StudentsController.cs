﻿using StudentCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentCatalog.Controllers
{
    public class StudentsController : Controller
    {
       
   
        // GET: Students
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult Create (Models.Student student)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext  db= new ApplicationDbContext();
                db.Students.Add(student);
                db.SaveChanges();
                
                return View("Thanks");
            }


            return View();

          

        }

    }
}