﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentCatalog.Models
{
    public class Student
    {
        public int Id { get; set; }
 
        [Required(ErrorMessage = "This need to be filled")]
        public String  FirstName { get; set; }

        [Required(ErrorMessage ="This need to be filled")]
        public String LastName { get; set; }



        [Required (ErrorMessage =" this need to be filled")]
        [EmailAddress(ErrorMessage = "This isn't a valid EmailAdress")]
        public String Email { get; set; }

        [Required(ErrorMessage = " the number isn't valid")]
        public String Mobile { get; set; }


    }
}